#include "bmp_manager.h"
#include "header.h"

//size_t fread(void *буфер, size_t колич_байт, size_t счетчик, FILE *уф);
enum READ_HEADER_STATUS read_header(FILE *file, struct bmp_header *header){
    if(fread(header, sizeof(struct bmp_header), 1, file) != 0){
        return HEADER_READ_OK;
    } else {
        return HEADER_READ_ERROR;
    }
}

//size_t fwrite(const void *буфер, size_t колич_байт, size_t счетчик, FILE *уф);
enum WRITE_HEADER_STATUS write_header(FILE *out, struct bmp_header header){
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 0){
        return HEADER_WRITE_OK;
    } else {
        return HEADER_WRITE_ERROR;
    }
}
//создаем хэдер 3 версии (40 байт)
struct bmp_header new_header(struct image *img){
    struct bmp_header header = {
            //тип файла (BM в ASCII),
            .bfType = 0x4D42,
            //размер файла = размер хэдера + размер картинки
            .bfileSize =  (sizeof (struct pixel) * (*img).width + padding((*img).width)) * (*img).height + sizeof(struct bmp_header),
            //обязательно следующие 4 бита заполняются нулями
            .bfReserved =  0,
            //положение данных о пикс. от начала файла
            //т.е. где начинается data_img
            .bOffBits = sizeof(struct bmp_header),
            //размер блока (версия), по заданию 3 версия - 40 байт
            .biSize = 40,
            //ширина изображения
            .biWidth = (*img).width,
            //высота
            .biHeight = (*img).height,
            //знач. формата (для bmp = 1)
            .biPlanes = 1,
            //кол-во бит на пикселей (по зад. 24)
            .biBitCount = 24,
            // метод компрессии
            .biCompression = 0,
            //размер изображ.
            .biSizeImage = (sizeof (struct pixel) * (*img).width + padding((*img).width)) * (*img).height,
            //ppm по горизонтали
            .biXPelsPerMeter = 0,
            //ppm по вертикали
            .biYPelsPerMeter = 0,
            //размер таблицы цветов в ячейках = piClrUsed
            .biClrUsed = 0,
            //количество ячеек от начала таблицы
            // цветов до последней используемой (включая её саму)
            .biClrImportant = 0
    };
    return header;
}
