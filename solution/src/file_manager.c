#include "file_manager.h"

enum FILE_STATUS open_file(FILE **file, char *file_name, char *mode){
    *file = fopen(file_name, mode);
    if (*file){
        return OPEN_OK;
    } else{
        return OPEN_ERROR;
    }
}
enum FILE_STATUS close_file(FILE *file){
    if (fclose(file)){
        return CLOSE_ERROR;
    } else {
        return CLOSE_OK;
    }
}
