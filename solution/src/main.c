#include "bmp_manager.h"
#include "file_manager.h"
#include "rotate.h"
#include <stdio.h>

int main( int argc, char **argv ) {
    if (argc != 3) {
        fprintf(stderr, "Вы должны ввести 2 аргумента");
        return 1;
    }
    struct image img = {0};

    FILE *input;
    FILE *output;

    enum FILE_STATUS fileStatus;

    fileStatus = open_file(&input, argv[1], "rb");
    if (fileStatus != OPEN_OK){
        return fileStatus;
    }

    enum READ_STATUS read_status;

    read_status = from_bmp(input, &img);
    if (read_status != READ_OK){
        delete_image(&img);
        return read_status;
    }

    //картинка в image, поэтому исходный файл больше не нужен
    fileStatus = close_file(input);
    if (fileStatus != CLOSE_OK){
        delete_image(&img);
        return fileStatus;
    }

    //Переворачиваем картинку, собственно, зачем мы здесь и собрались
    struct image rotImg = rotate(img);


    fileStatus = open_file(&output, argv[2], "wb");
    if (fileStatus != OPEN_OK){
        delete_image(&rotImg);
        delete_image(&img);
        return fileStatus;
    }

    enum WRITE_STATUS write_status;

    write_status = to_bmp(output, &rotImg);
    if (write_status != WRITE_OK){
        delete_image(&rotImg);
        delete_image(&img);
        return write_status;
    }

    //картинка в image, поэтому исходный файл больше не нужен
    fileStatus = close_file(output);
    if (fileStatus != CLOSE_OK){
        delete_image(&rotImg);
        delete_image(&img);
        return fileStatus;
    }

    //не забываем подчистить память

    delete_image(&rotImg);
    delete_image(&img);
    return 0;
}
