#include "image.h"

struct image create_image(size_t width, size_t height){
    struct image img = {0};

    //выделяем память в размере кол-во пикселей, умнож. на размер пикс.
    struct pixel *data = malloc(width*height*sizeof(struct pixel));
    if (data) {
        img.width = width;
        img.height = height;
        img.data = data;
    }
    return img;
}
//освобождаем место в памяти, которое занимала картинка
void delete_image(struct image *img){
    free((*img).data);
}
