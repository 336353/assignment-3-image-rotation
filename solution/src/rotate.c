#include "rotate.h"

struct image rotate(struct image img) {
    struct image rotImg = create_image(img.height, img.width);
    for (size_t i = 0; i < img.height; i++) {
        for (size_t j = 0; j < img.width; j++) {
            rotImg.data[img.height * j + (img.height - 1 - i)] = img.data[i * img.width + j];
        }
    }
    return rotImg;
}
