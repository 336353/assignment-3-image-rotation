#include "bmp_manager.h"
//если ширина не кратна 4, то она дополняется мусорн. байтами
//до ближ. кратн. 4 числа, поэтому нам нужно определить,
//сколько будет мусорн. байтов

uint16_t padding(size_t width){
    uint16_t bytes_without_padding = (width * sizeof(struct pixel)) % 4;
    uint16_t size_of_padding = (4 - bytes_without_padding) % 4;
    return size_of_padding;
}

//размер картинки = (ширина в байтах + паддинг) * высота в байтах
uint32_t get_img_size(struct image *img){
    uint32_t img_size = (sizeof (struct pixel) * (*img).width + padding((*img).width)) * (*img).height;
    return img_size;
}

/*int fseek( FILE * filestream, long int offset, int origin );
filestream
Указатель на объект типа FILE, идентифицируемый поток
offset - Количество байт для смещения, относительно некоторого
положения указателя.
origin - Позиция указателя, относительно которой
будет выполняться смещение. Одна из констант: SEEK_SET - нач. файла,
SEEK_CUR - текущ. полож. файла, SEEK_END - конец файла

 */

enum READ_STATUS read_img_data(FILE *in, struct image *img){

    for (size_t i = 0; i < (*img).height; i++){
        if (fread((*img).data + (*img).width * i,  sizeof (struct pixel), (*img).width, in) != (*img).width){
            return DATA_READ_ERROR;
        }
        if (fseek(in, padding((*img).width), SEEK_CUR) != 0 ){
            return DATA_READ_ERROR;
        }
    }
    return DATA_READ_OK;
}


enum READ_STATUS from_bmp( FILE* in, struct image* img ) {
    struct bmp_header header = {0};
    if (read_header(in, &header) == HEADER_READ_OK) {
        *img = create_image(header.biWidth, header.biHeight);
        return read_img_data(in, img);
    }
    return READ_INVALID_HEADER;
}


enum WRITE_STATUS write_img_data(FILE *out, struct image *img){

    for (size_t i = 0; i < (*img).height; i++){
        if (fwrite((*img).data + (*img).width * i,  sizeof (struct pixel), (*img).width, out) != (*img).width){
            return DATA_WRITE_ERROR;
        }
        if (fseek(out, padding((*img).width), SEEK_CUR) != 0 ){
            return DATA_WRITE_ERROR;
        }
    }
    return DATA_WRITE_OK;
}


enum WRITE_STATUS to_bmp( FILE* out, struct image *img ){
    struct bmp_header header = new_header(img);
    if (write_header(out, header) == HEADER_WRITE_OK){
        return write_img_data(out, img);
    }
    return WRITE_INVALID_HEADER;
}
