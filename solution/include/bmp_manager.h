#ifndef IMAGE_TRANSFORMER_BMP_MANAGER_H
#define IMAGE_TRANSFORMER_BMP_MANAGER_H

#include "header.h"
#include "image.h"
#include <stdint.h>
#include <stdio.h>

uint16_t padding(size_t width);

uint32_t get_img_size(struct image *img);


#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

/*  deserializer   */
enum READ_STATUS  {
    READ_OK = 0,
    DATA_READ_OK = 0,
    DATA_READ_ERROR = 21,
    READ_INVALID_HEADER = 22
};

enum READ_STATUS read_img_data(FILE *in, struct image *img);

enum READ_STATUS from_bmp( FILE* in, struct image* img );

/*  serializer   */
enum  WRITE_STATUS  {
    WRITE_OK = 0,
    DATA_WRITE_OK = 0,
    DATA_WRITE_ERROR = 31,
    WRITE_INVALID_HEADER = 32
};

enum WRITE_STATUS write_img_data(FILE *out, struct image *img);

enum WRITE_STATUS to_bmp( FILE* out, struct image *img );

#endif //IMAGE_TRANSFORMER_BMP_MANAGER_H
