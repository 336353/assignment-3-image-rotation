#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#include "stdint.h"
#include <malloc.h>

/*Каждый пиксель задается структурой размером 3 байта
Если ширина изображения не кратна четырем, то строчки
пикселей идут без пропуска, иначе ширина дополняется
мусорными байтами => нужно упаковать структуру пикселя, чтобы
 это избежать*/
#pragma pack(push, 1)
struct pixel { uint8_t b, g, r; };
#pragma pack(pop)

struct image {
    size_t width, height;
    struct pixel* data;
};

struct image create_image(size_t width, size_t height);

void delete_image(struct image *img);

#endif //IMAGE_TRANSFORMER_IMAGE_H
