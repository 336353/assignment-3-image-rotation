#ifndef IMAGE_TRANSFORMER_HEADER_H
#define IMAGE_TRANSFORMER_HEADER_H
#include "image.h"
#include <stdint.h>
#include <stdio.h>


enum READ_HEADER_STATUS {
    HEADER_READ_OK = 0,
    HEADER_READ_ERROR = 41
};


enum  WRITE_HEADER_STATUS {
    HEADER_WRITE_OK = 0,
    HEADER_WRITE_ERROR = 51
};


struct bmp_header new_header(struct image *img);

enum READ_HEADER_STATUS read_header(FILE *file, struct bmp_header *header);

enum WRITE_HEADER_STATUS write_header(FILE *out, struct bmp_header header);
#endif //IMAGE_TRANSFORMER_HEADER_H
