#ifndef IMAGE_TRANSFORMER_FILE_MANAGER_H
#define IMAGE_TRANSFORMER_FILE_MANAGER_H
#include "stdio.h"

//Коды ошибок, возникающих при чтении/записи файлов
enum FILE_STATUS {
    OPEN_OK = 0,
    OPEN_ERROR = 11,
    CLOSE_OK = 0,
    CLOSE_ERROR = 12
};

enum FILE_STATUS open_file(FILE **file, char *file_name, char *mode);
enum FILE_STATUS close_file(FILE *file);

#endif //IMAGE_TRANSFORMER_FILE_MANAGER_H
