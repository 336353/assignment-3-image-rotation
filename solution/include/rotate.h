#include "image.h"

#ifndef IMAGE_TRANSFORMER_ROTATE_H
#define IMAGE_TRANSFORMER_ROTATE_H

/* создаёт копию изображения, которая повёрнута на 90 градусов */
struct image rotate( struct  image img);

#endif //IMAGE_TRANSFORMER_ROTATE_H
